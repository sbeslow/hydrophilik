package utils;

import org.joda.time.DateTimeZone;

public class Config {

	public final static DateTimeZone chiTimeZone = DateTimeZone.forID("America/Chicago");
	
	//public static String NCDC_URL="http://cdo.ncdc.noaa.gov/qclcd/QCLCD?prior=N";
	public static String NCDC_URL="http://cdo.ncdc.noaa.gov";
}
