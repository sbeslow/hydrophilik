package models;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.joda.time.DateTime;

import play.db.ebean.Model;
import utils.TimeUtils;

@Entity
public class RainEvent extends Model implements Comparable<RainEvent>{
	
	@Id
	public Long id = null;

	public Integer year;
	public Integer month;
	public Integer day;
	public String startTime;
	public String endTime;
	public String location;
	public Double precipiationInches;
	


	public RainEvent(Integer year, Integer month, Integer day,
			String startTime, String endTime, String location,
			Double precipiationInches) {
		this.year = year;
		this.month = month;
		this.day = day;
		this.startTime = startTime;
		this.endTime = endTime;
		this.location = location;
		this.precipiationInches = precipiationInches;
	}

	@Override
	public int compareTo(RainEvent o) {
		
		if (null == o.getStartTime())
			return -1;
		else if (null == getStartTime())
			return 1;
		else if (getStartTime().isBefore(o.getStartTime()))
			return -1;
		else if (o.getStartTime().isBefore(getStartTime()))
			return 1;
		else
			return 0;
	}

	public DateTime getStartTime() {
		return TimeUtils.convertStringToJoda(startTime);
	}

	public DateTime getEndTime() {
		return TimeUtils.convertStringToJoda(endTime);
	}

	public String getLocation() {
		return location;
	}

	public Double getPrecipiationInches() {
		return precipiationInches;
	}
	
	public static Finder<Long, RainEvent> find() {
		return new Finder<Long, RainEvent>(Long.class, RainEvent.class);
	}
	
	public String print() {
		return "Rain Event on " +
				Integer.toString(month) + "/" +
				Integer.toString(day) + "/" +
				Integer.toString(year) + "/";
	}
}