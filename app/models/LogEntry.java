package models;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.joda.time.LocalDate;

import play.db.ebean.Model;

@Entity
public class LogEntry extends Model{

	@Id
	public long id;
	
	public LocalDate timestamp;
	
	public String message;
}
