package controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.LocalDate;

import models.RainEvent;
import play.mvc.Controller;
import play.mvc.Result;
import utils.TimeUtils;

public class RainEvents extends Controller {
	
	private static List<RainEvent> rainEvents = null;

	public static Result showDates(String date, Integer numDays) {
		if (null == rainEvents) {
			//DataLoader.loadData();
			if (null == rainEvents)
				return badRequest("Unable to load data");
		}
		
		if ((7 < numDays) || (1 > numDays)) {
			return badRequest("Maximum of 7 days and minimum of 1 is allowed");
		}
		
		LocalDate startDate = TimeUtils.createDateTimeFromCsv(date, 1).toLocalDate();
		LocalDate endDate = startDate.plusDays(numDays - 1);
		
		List<RainEvent> relevantEvents = new ArrayList<RainEvent>();
		for (RainEvent rainEvent : rainEvents) {
			LocalDate rainEventDate = rainEvent.getStartTime().toLocalDate();
			if ((rainEventDate.equals(startDate)) || (rainEventDate.equals(endDate)) ||
					((rainEventDate.isAfter(startDate)) && (rainEventDate.isBefore(endDate)))) {
				relevantEvents.add(rainEvent);
			}
		}
		
		Collections.sort(relevantEvents);

		String heading = "Hourly precipitation for " + TimeUtils.convertDateToString(startDate);
		if (numDays > 1) {
			heading += " thru " + TimeUtils.convertDateToString(endDate);
		}
		
		return ok(views.html.showRains.render(relevantEvents, heading));
	}
	
	
	public static void setEventsFromCsv(List<RainEvent> csvEvents) {
		rainEvents = csvEvents;
	}
	
	public static Map<Integer, RainEvent> findByDateAndLocation(String year, String month, String location) {
		List<RainEvent> events = RainEvent.find().where().eq("year", year).eq("month", month)
				.eq("location",location).findList();
		Map<Integer, RainEvent> eventMap = new HashMap<Integer,RainEvent>();
		for (RainEvent event : events) {
			eventMap.put(event.day, event);
		}
		return eventMap;
	}
}
