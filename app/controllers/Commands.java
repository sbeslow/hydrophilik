package controllers;

import org.joda.time.LocalDate;

import com.jaunt.Document;
import com.jaunt.JauntException;
import com.jaunt.UserAgent;
import com.jaunt.component.Form;

import dataManager.NCDCPageParser;
import play.mvc.Controller;
import play.mvc.Result;

public class Commands extends Controller {
	
	public static final String ncdcUrl = "http://cdo.ncdc.noaa.gov/qclcd/QCLCD?prior=N";
	
	
	public static Result scrapeNCDC(String startDateArg, String endDateArg) {

		try{
			
			UserAgent userAgent = new UserAgent();
			
			String [] dateSplit = startDateArg.split("-");
			LocalDate date = new LocalDate(Integer.parseInt(dateSplit[1]),
					Integer.parseInt(dateSplit[0]), 1);

			dateSplit = endDateArg.split("-");
			LocalDate endDate = new LocalDate(Integer.parseInt(dateSplit[1]),
					Integer.parseInt(dateSplit[0]), 1);
			
			while (date.isBefore(endDate)) {
				  userAgent.visit(ncdcUrl);
				  
				  // Select Illinois
				  Document il = userAgent.doc;
				  il.select("Desired Station is Located", "Illinois");
				  il.submit();
				  
				  Form form = userAgent.doc.forms[0];
				  form.setSelect("callsign", "MDW");
				  
				  form.submit();
				  form = userAgent.doc.forms[0];

				  Integer monthInt = Integer.parseInt(date.monthOfYear().getAsString());
				  String monthStr = Integer.toString(monthInt);
				  if (monthInt < 10) {
					  monthStr = "0" + monthStr;
				  }
				  
				  String baseCode = "14819" + date.year().getAsString() + monthStr;
				  form.setSelect("VARVALUE", baseCode);
				  
				  System.out.println("Submitting basecode: " + baseCode);

				  form.submit();
				  form = userAgent.doc.forms[0];
				  form.setSelect("reqday", "E");
				  form.setRadio("which","ASCII Download (Hourly Precip.)");
				  form.submit();
			  
				  NCDCPageParser.parseNcdcDataPage(userAgent.doc.innerHTML());

				
				date = date.plusMonths(1);
				
			}
			
			
			  

			}
			catch(JauntException e){
			  System.err.println(e);
			  return ok("Failure");
			}
		
		return ok("Success");
	}
}
