package dataManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import models.RainEvent;

import org.joda.time.DateTime;

import controllers.RainEvents;
import utils.TimeUtils;

public class NCDCPageParser {

	private static List<RainEvent> getRainEventsFromCsvLine(String csvLine) {
		List<RainEvent> retVal = new ArrayList<RainEvent>();
		String [] columns = csvLine.split(",");
		
		if (50 != columns.length) {
			return null;
		}
		
		try {
			Integer.parseInt(columns[0]);
		}
		catch (Exception e) {
			return null;
		}
		
		String location = columns[0];
		String dateStr = columns[1];
		int year = Integer.parseInt(dateStr.substring(0,4));
		int month = Integer.parseInt(dateStr.substring(4, 6));
		int day = Integer.parseInt(dateStr.substring(6));

		int placeHolder = 2;
		while (placeHolder < columns.length) {
			int hour = placeHolder / 2;

			DateTime endTime = TimeUtils.createDateTimeFromCsv(dateStr, hour);
			if (null == endTime) {
				placeHolder += 2;
				continue;
			}

			DateTime startTime = endTime.minusHours(1);

			String inchesStr = columns[placeHolder];
			Double inches;
			try {
				inches = Double.parseDouble(inchesStr);
			}
			catch(Exception e) {
				inches = 0.0;
			}
			
			String startStr = TimeUtils.convertTimeToString(startTime.toLocalTime());
			String endStr = TimeUtils.convertTimeToString(endTime.toLocalTime());

			RainEvent event = new RainEvent(year, month, day, startStr, endStr, location, inches);
			retVal.add(event);
			
			placeHolder += 2;
		}

		if (24 != retVal.size()) {
			System.out.println("Incorrect line of size " + retVal.size() + " for:\n" + csvLine);
		}

		return retVal;
	}
	
	public static void parseNcdcDataPage(String rawHtml) {
		String [] lines = rawHtml.split("\n");
		
		Map<Integer, RainEvent> dbEventsForMonth = null;
		
		
		for (String line : lines) {
			List<RainEvent> events = getRainEventsFromCsvLine(line);
			if (null == events)
				continue;
			
			if (null == dbEventsForMonth) {
				RainEvent firstEvent = events.get(0);
				String year = Integer.toString(firstEvent.year);
				String month = Integer.toString(firstEvent.month);
				String location = firstEvent.location;
				
				dbEventsForMonth = RainEvents.findByDateAndLocation(year, month, location);

			}
			
			for (RainEvent event : events) {
				if (null == dbEventsForMonth.get(event.day)) {
					System.out.println(event.print());
					event.save();
				}
				else {
					RainEvent updateEvent = dbEventsForMonth.get(event.day);
					updateEvent.precipiationInches = event.precipiationInches;
					updateEvent.update();
				}
			}
			System.out.println("Got lines: " + events.size());
		}
	}
}
