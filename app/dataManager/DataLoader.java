package dataManager;
/*
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import models.RainEvent;

import org.joda.time.DateTime;

import controllers.RainEvents;
import play.Play;
import utils.TimeUtils;
*/
public class DataLoader {
/*

	public static void loadData() {
		InputStream is = Play.application().classloader().getResourceAsStream("resources/rainRawOhare.txt");
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String line = null;
		
		try {
			// Ignore the first line, since it is the headings.  Leave it in the CSV for reference.
			line = br.readLine();
			
			List<RainEvent> events = new ArrayList<RainEvent>(); 
	
			while ((line = br.readLine()) != null) {
				List<RainEvent> newEvents = null;
				try {
					newEvents = getRainEventsFromCsvLine(line);
				}
				catch (Exception e) {
					System.out.println("Failed to parse line:\n" + line);
				}
				for (RainEvent newEvent : newEvents) {
					events.add(newEvent);
				}
			}
			
			RainEvents.setEventsFromCsv(events);
		}
		catch (Exception e) {
			System.out.println("Uh oh");
		}
		finally {
			try {
				if (null != br) br.close();
				if (null != is) is.close();
			} catch (Exception e) {}
		}
	}
	
	private static List<RainEvent> getRainEventsFromCsvLine(String csvLine) {
		List<RainEvent> retVal = new ArrayList<RainEvent>();
		String [] columns = csvLine.split(",");
		
		// TODO: Check size of columns here
		
		String location = columns[0];
		String dateStr = columns[1];

		int placeHolder = 2;
		while (placeHolder < columns.length) {
			int hour = placeHolder / 2;
			DateTime endTime = TimeUtils.createDateTimeFromCsv(dateStr, hour);
			if (null == endTime) {
				placeHolder += 2;
				continue;
			}

			DateTime startTime = endTime.minusHours(1);

			String inchesStr = columns[placeHolder];
			Double inches;
			try {
				inches = Double.parseDouble(inchesStr);
			}
			catch(Exception e) {
				inches = 0.0;
			}

			// TODO: Not doing anything here
			//RainEvent event = new RainEvent(location, startTime, endTime, inches);
			//retVal.add(event);
			
			placeHolder += 2;
		}

		if (24 != retVal.size()) {
			System.out.println("Incorrect line of size " + retVal.size() + " for:\n" + csvLine);
		}

		return retVal;
	}
*/
}
