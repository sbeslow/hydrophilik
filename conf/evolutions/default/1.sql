# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table rain_event (
  id                        bigint not null,
  year                      integer,
  month                     integer,
  day                       integer,
  start_time                varchar(255),
  end_time                  varchar(255),
  location                  varchar(255),
  precipiation_inches       double,
  constraint pk_rain_event primary key (id))
;

create sequence rain_event_seq;




# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists rain_event;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists rain_event_seq;

