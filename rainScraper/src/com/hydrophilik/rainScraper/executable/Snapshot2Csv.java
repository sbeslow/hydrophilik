package com.hydrophilik.rainScraper.executable;

import com.hydrophilik.rainScraper.actor.RainEvent;
import com.hydrophilik.rainScraper.controllers.RainEvents;
import com.hydrophilik.rainScraper.utils.TimeUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Created by scottbeslow on 8/18/14.
 */
public class Snapshot2Csv {

    private static String fileNameAndPath = "/Users/scottbeslow/Downloads/rainEvents.csv";

    public static void main(String[] args) {

        BufferedWriter bw = null;
        FileWriter fw = null;

        try {

            File file = new File(fileNameAndPath);

            // if file doesnt exists, then create it
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();

            fw = new FileWriter(file.getAbsoluteFile());
            bw = new BufferedWriter(fw);

            bw.write("year,month,day,startTime,endTime,location,precipitationInches\n");

            List<RainEvent> events = RainEvents.retrieveAllEvents();
            for (RainEvent event : events) {
                bw.write(event.csvOut() + "\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                bw.close();
                fw.close();
            }catch (Exception e) {}
    }

    }
}
