package com.hydrophilik.rainScraper.executable;

import com.hydrophilik.rainScraper.action.NCDCSiteScrape;
import com.hydrophilik.rainScraper.actor.StationTable;
import com.hydrophilik.rainScraper.utils.TimeUtils;
import org.joda.time.LocalDate;

import java.util.Map;

/**
 * Created by scottbeslow on 8/18/14.
 */
public class ScrapeSpecific {

    private static final String startMonth = "20050101";
    private static final String endMonth = "20140831";

    private static String callSign = "DPA";

    public static void main(String[] args) {

        LocalDate startDate = (TimeUtils.createDateTimeFromCsv(startMonth, 1)).toLocalDate();
        LocalDate endDate = (TimeUtils.createDateTimeFromCsv(endMonth, 1)).toLocalDate();

        Map<String, String> stationTable = StationTable.build();
        String stationId = stationTable.get(callSign);
        if (null == stationId) {
            System.out.println("Unable to find stationId");
            return;
        }

        NCDCSiteScrape.scrapeTimespan(startDate, endDate, callSign, stationId);


    }
}
