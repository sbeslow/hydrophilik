package com.hydrophilik.rainScraper.executable;

import com.hydrophilik.rainScraper.actor.FrequencyEvent;
import com.hydrophilik.rainScraper.actor.RainEvent;
import com.hydrophilik.rainScraper.actor.StationTable;
import com.hydrophilik.rainScraper.actor.ThresholdTable;
import com.hydrophilik.rainScraper.controllers.RainEvents;
import com.hydrophilik.rainScraper.db.DbConnector;

import java.util.*;

/**
 * Created by scottbeslow on 8/18/14.
 */
public class FindEvents {

    //private final static String location = "14819";
    private final static String callSign = "ORD";


    public final static Map<String, Double> thresholdTable = ThresholdTable.build();

    public static void main(String[] args) {
        List<Integer> yearList = Arrays.asList(100, 50, 25, 10, 5, 2, 1);
        List<Integer> hourList = Arrays.asList(24, 12,6,3,2,1);

        Map<String, String> stationTable = StationTable.build();
        String location = stationTable.get(callSign);

        List<RainEvent> rainEvents = RainEvents.retrieveEventsByLocation(location);

        Collections.sort(rainEvents);

        List<FrequencyEvent> freqEvents = new ArrayList<FrequencyEvent>();

        for (Integer yearInt : yearList) {
            for (Integer hour : hourList) {
                List<FrequencyEvent> newEvents = findEvent(rainEvents, hour, yearInt, location);
                for (FrequencyEvent freqEvent : newEvents) {
                    List<FrequencyEvent> overlaps = freqEvent.overlapsWith(freqEvents);
                    if (0 == overlaps.size()) {
                        freqEvents.add(freqEvent);
                        continue;
                    }
                    for (FrequencyEvent overlappedEvent : overlaps) {
                        if (overlappedEvent.getPrecipitation() >= freqEvent.getPrecipitation())
                            continue;
                        if (overlappedEvent.getReturnInterval() >= freqEvent.getReturnInterval())
                            continue;
                        freqEvents.remove(overlappedEvent);
                        freqEvents.add(freqEvent);
                    }
                }
            }
        }

        for (FrequencyEvent frEvent : freqEvents) {
            System.out.println(frEvent.print());
        }

    }

    public static List<FrequencyEvent> findEvent(List<RainEvent> rainEvents, int hours, int yearStorm, String location) {

        Double threshold = thresholdTable.get(hours + ":" + yearStorm);

        if ((null == threshold) || (0 == threshold)) {
            System.out.println("Unable to retrieve threshold for " + hours + " " + yearStorm);
            return new ArrayList<FrequencyEvent>(1);
        }

        List<FrequencyEvent> retVal = new ArrayList<FrequencyEvent>();

        for (RainEvent event : rainEvents) {
            if (0 == event.getPrecipitationInches()) {
                continue;
            }

            int place = rainEvents.indexOf(event);
            double totalPrecip = 0;
            for (int i = 0; i < hours; i++) {
                totalPrecip += rainEvents.get(place + i).getPrecipitationInches();
            }

            if (totalPrecip >= threshold) {
                //System.out.println(yearStorm + "-year, " + hours + "-hour event found with precip: " + totalPrecip + "\n" + event.print());
                FrequencyEvent freqEvent = new FrequencyEvent(event.getStartTime(),
                        event.getStartTime().plusHours(hours), totalPrecip, yearStorm);
                retVal.add(freqEvent);
            }
        }

        return retVal;
    }

}