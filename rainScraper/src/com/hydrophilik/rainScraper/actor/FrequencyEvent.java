package com.hydrophilik.rainScraper.actor;

import com.hydrophilik.rainScraper.utils.TimeUtils;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by scottbeslow on 8/18/14.
 */
public class FrequencyEvent implements Comparable<FrequencyEvent> {

    private DateTime startTime;
    private DateTime endTime;
    private Double precipitation;
    private int returnInterval;

    public FrequencyEvent(DateTime startTime, DateTime endTime, Double precipitation, int returnInterval) {

        this.startTime = startTime;
        this.endTime = endTime;
        this.precipitation = precipitation;
        this.returnInterval = returnInterval;
    }

    public DateTime getStartTime() {
        return startTime;
    }

    public DateTime getEndTime() {
        return endTime;
    }

    public Double getPrecipitation() {
        return this.precipitation;
    }
    public int getReturnInterval() {
        return this.returnInterval;
    }

    @Override
    public int compareTo(FrequencyEvent o) {
        if ((null == o.getStartTime()) || (null == o.getEndTime()))
            return -1;
        else if ((null == getStartTime()) || (null == getEndTime()))
            return 1;
        else if (getStartTime().isBefore(o.getStartTime()))
            return -1;
        else if (o.getStartTime().isBefore(getStartTime()))
            return 1;
        else
            return 0;
    }

    public List<FrequencyEvent> overlapsWith(List<FrequencyEvent> freqEvents) {
        List<FrequencyEvent> retVal = new ArrayList<FrequencyEvent>(freqEvents.size());

        for (FrequencyEvent event : freqEvents) {
            if ((getStartTime().isAfter(event.getEndTime())) || (getEndTime().isBefore(event.getStartTime())))
                continue;

            retVal.add(event);
        }

        return retVal;
    }

    public String print() {
        String retVal = Integer.toString(returnInterval) + "- year event received " + Double.toString(precipitation) +
                " inches of rain from " + TimeUtils.convertJodaToString(this.getStartTime()) + " to " +
                TimeUtils.convertJodaToString(this.getEndTime());

        return retVal;
    }
}
