package com.hydrophilik.rainScraper.actor;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by scottbeslow on 8/19/14.
 */
public class StationTable {

    public static Map<String, String> build() {
        // Key is callsign, value is stationId
        Map<String, String> retVal = new HashMap<String, String>();
        retVal.put("ORD", "94846");
        retVal.put("IGQ", "04879");
        retVal.put("ARR", "04808");
        retVal.put("PWK", "04838");
        retVal.put("LOT", "04831");
        retVal.put("UGN", "14880");
        retVal.put("DPA", "94892");



        return retVal;

    }
}
