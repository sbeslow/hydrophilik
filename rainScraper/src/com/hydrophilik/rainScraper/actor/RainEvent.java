package com.hydrophilik.rainScraper.actor;

import com.hydrophilik.rainScraper.utils.TimeUtils;
import org.joda.time.DateTime;

public class RainEvent implements Comparable<RainEvent>{

	private Integer year;
    private Integer month;
    private Integer day;
    private String startTime;
    private String endTime;
    private String location;

    private Double precipitationInches;


	public RainEvent(Integer year, Integer month, Integer day,
			String startTime, String endTime, String location,
			Double precipitationInches) {
		this.year = year;
		this.month = month;
		this.day = day;
		this.startTime = startTime;
		this.endTime = endTime;
		this.location = location;
		this.precipitationInches = precipitationInches;
	}

	@Override
	public int compareTo(RainEvent o) {
		
		if (null == o.getStartTime())
			return -1;
		else if (null == getStartTime())
			return 1;
		else if (getStartTime().isBefore(o.getStartTime()))
			return -1;
		else if (o.getStartTime().isBefore(getStartTime()))
			return 1;
		else
			return 0;
	}

	public DateTime getStartTime() {
        String monthStr = Integer.toString(month);
        if (month < 10)
            monthStr = "0" + monthStr;
        String dayStr = Integer.toString(day);
        if (day < 10)
            dayStr  = "0" + dayStr;
        String dateStr = Integer.toString(year) + monthStr + dayStr;
        int hour = Integer.parseInt(startTime.split(":")[0]);
        DateTime dt = TimeUtils.createDateTimeFromCsv(dateStr, hour);
		return dt;
	}
	
	public String print() {
        String retVal = "Rain Event on " +
				Integer.toString(month) + "/" +
				Integer.toString(day) + "/" +
				Integer.toString(year) + "/";

        retVal +="\n";

        retVal += this.startTime + " thru " + this.endTime;
        retVal += "\n";
        retVal += this.precipitationInches.toString() + " inches";

        return retVal;
	}

    public String sqlParagraph() {
        return "(NULL," + this.year + "," + this.month + "," + this.day + ",'" +
                this.startTime + "','" + this.endTime + "','" + this.location + "'," + this.precipitationInches + ")";
    }

    public Double getPrecipitationInches() {
        return precipitationInches;
    }

    public String csvOut() {
        return Integer.toString(this.year) + "," +
                Integer.toString(this.month) + "," +
                Integer.toString(this.day) + "," +
                TimeUtils.convertTimeToString(this.getStartTime().toLocalTime()) + "," +
                TimeUtils.convertTimeToString(this.getStartTime().toLocalTime().plusHours(1)) + "," +
                this.location + "," + Double.toString(this.precipitationInches);

    }
}