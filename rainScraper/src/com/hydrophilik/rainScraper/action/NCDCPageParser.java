package com.hydrophilik.rainScraper.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import com.hydrophilik.rainScraper.actor.RainEvent;
import com.hydrophilik.rainScraper.db.DbConnector;
import com.hydrophilik.rainScraper.utils.Config;
import com.hydrophilik.rainScraper.utils.TimeUtils;
import org.joda.time.DateTime;


public abstract class NCDCPageParser {

    public static void parseNCDCDataPage(String rawHtml) {
        String [] lines = rawHtml.split("\n");

        List<RainEvent> allEvents = new ArrayList<RainEvent>();
        for (String line : lines) {
            List<RainEvent> events = getRainEventsFromCsvLine(line);
            if (null == events)
                continue;

            for (RainEvent event : events) {
                allEvents.add(event);
            }
        }

        if (0 == allEvents.size()) {
            System.out.println("Retrieved no records");
            return;
        }

        DbConnector dbConnector = null;
        try {
            dbConnector = new DbConnector();
            dbConnector.addEvents(allEvents);

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

	private static List<RainEvent> getRainEventsFromCsvLine(String csvLine) {
		List<RainEvent> retVal = new ArrayList<RainEvent>();
		String [] columns = csvLine.split(",");
		
		if (50 != columns.length) {
			return null;
		}
		
		try {
			Integer.parseInt(columns[0]);
		}
		catch (Exception e) {
			return null;
		}
		
		String location = columns[0];
		String dateStr = columns[1];
		int year = Integer.parseInt(dateStr.substring(0,4));
		int month = Integer.parseInt(dateStr.substring(4, 6));
		int day = Integer.parseInt(dateStr.substring(6));

        if (9 == day) {
            System.out.println("Leap Day");
        }

		int placeHolder = 2;
		while (placeHolder < columns.length) {
			int hour = placeHolder / 2;

			DateTime endTime = TimeUtils.createDateTimeFromCsv(dateStr, hour);
			if (null == endTime) {
				placeHolder += 2;
				continue;
			}

			DateTime startTime = endTime.minusHours(1);

            // Is this a leap day
            if (startTime.getZone().nextTransition(startTime.getMillis()) < endTime.getMillis()) {
                System.out.println("Detected daylight savings now");
            }

			String inchesStr = columns[placeHolder];
			Double inches;
			try {
				inches = Double.parseDouble(inchesStr);
			}
			catch(Exception e) {
				inches = 0.0;
			}
			
			String startStr = TimeUtils.convertTimeToString(startTime.toLocalTime());
			String endStr = TimeUtils.convertTimeToString(endTime.toLocalTime());

			RainEvent event = new RainEvent(year, month, day, startStr, endStr, location, inches);
			retVal.add(event);
			
			placeHolder += 2;
		}

		if (24 != retVal.size()) {
			System.out.println("Incorrect number of days for: " + month + "/" + day + "/" + year);
		}

		return retVal;
	}
	

}
