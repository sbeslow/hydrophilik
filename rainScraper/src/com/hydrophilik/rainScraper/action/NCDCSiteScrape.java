package com.hydrophilik.rainScraper.action;

import com.hydrophilik.rainScraper.utils.Config;
import com.jaunt.Document;
import com.jaunt.JauntException;
import com.jaunt.UserAgent;
import com.jaunt.component.Form;
import org.joda.time.LocalDate;

/**
 * Created by scottbeslow on 8/17/14.
 */
public abstract class NCDCSiteScrape {

    public static String scrapeRestOfYear(LocalDate startDate, String callSign, String stationId) {

        try {

            UserAgent userAgent = new UserAgent();

            LocalDate date = startDate;

            LocalDate endDate = new LocalDate(date.year().get(), 12, 1);
            LocalDate today = new LocalDate();

            while ((false == date.isAfter(endDate)) && (false == date.isAfter(today))) {
                userAgent.visit(Config.NCDC_URL);

                // Select Illinois
                Document il = userAgent.doc;
                il.select("Desired Station is Located", "Illinois");
                il.submit();

                Form form = userAgent.doc.forms[0];
                form.setSelect("callsign", callSign);

                form.submit();
                form = userAgent.doc.forms[0];

                Integer monthInt = Integer.parseInt(date.monthOfYear().getAsString());
                String monthStr = Integer.toString(monthInt);
                if (monthInt < 10) {
                    monthStr = "0" + monthStr;
                }

                String baseCode = stationId + date.year().getAsString() + monthStr;
                form.setSelect("VARVALUE", baseCode);

                System.out.println("Submitting basecode: " + baseCode);

                form.submit();
                form = userAgent.doc.forms[0];
                form.setSelect("reqday", "E");
                form.setRadio("which", "ASCII Download (Hourly Precip.)");

                form.submit();

                NCDCPageParser.parseNCDCDataPage(userAgent.doc.innerHTML());

                date = date.plusMonths(1);

            }


        } catch (JauntException e) {
            System.err.println(e);
            return null;
        }
        return stationId;

    }

    public static String scrapeMonth(LocalDate startDate, String callSign, String stationId) {

        try {

            UserAgent userAgent = new UserAgent();

            LocalDate date = startDate;

            userAgent.visit(Config.NCDC_URL);

            // Select Illinois
            Document il = userAgent.doc;
            il.select("Desired Station is Located", "Illinois");
            il.submit();

            Form form = userAgent.doc.forms[0];
            form.setSelect("callsign", callSign);

            form.submit();
            form = userAgent.doc.forms[0];

            Integer monthInt = Integer.parseInt(date.monthOfYear().getAsString());
            String monthStr = Integer.toString(monthInt);
            if (monthInt < 10) {
                monthStr = "0" + monthStr;
            }

            String baseCode = stationId + date.year().getAsString() + monthStr;
            form.setSelect("VARVALUE", baseCode);

            System.out.println("Submitting basecode: " + baseCode);

            form.submit();
            form = userAgent.doc.forms[0];
            form.setSelect("reqday", "E");
            form.setRadio("which", "ASCII Download (Hourly Precip.)");

            form.submit();

            NCDCPageParser.parseNCDCDataPage(userAgent.doc.innerHTML());


        } catch (JauntException e) {
            System.err.println(e);
            return null;
        }
        return stationId;

    }

    public static void scrapeTimespan(LocalDate startDate, LocalDate endDate, String callSign, String stationId) {

        try {

            UserAgent userAgent = new UserAgent();

            LocalDate date = startDate;

            LocalDate today = new LocalDate();

            while ((false == date.isAfter(endDate)) && (false == date.isAfter(today))) {
                userAgent.visit(Config.NCDC_URL);

                // Select Illinois
                Document il = userAgent.doc;
                il.select("Desired Station is Located", "Illinois");
                il.submit();

                Form form = userAgent.doc.forms[0];
                form.setSelect("callsign", callSign);

                form.submit();
                form = userAgent.doc.forms[0];

                Integer monthInt = Integer.parseInt(date.monthOfYear().getAsString());
                String monthStr = Integer.toString(monthInt);
                if (monthInt < 10) {
                    monthStr = "0" + monthStr;
                }

                String baseCode = stationId + date.year().getAsString() + monthStr;
                form.setSelect("VARVALUE", baseCode);

                System.out.println("Submitting basecode: " + baseCode);

                form.submit();
                form = userAgent.doc.forms[0];
                form.setSelect("reqday", "E");
                form.setRadio("which", "ASCII Download (Hourly Precip.)");

                form.submit();

                NCDCPageParser.parseNCDCDataPage(userAgent.doc.innerHTML());

                date = date.plusMonths(1);

            }


        } catch (JauntException e) {
            System.err.println(e);
            return;
        }

    }
}