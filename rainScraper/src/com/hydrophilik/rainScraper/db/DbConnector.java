package com.hydrophilik.rainScraper.db;

import com.hydrophilik.rainScraper.actor.RainEvent;
import com.hydrophilik.rainScraper.utils.Config;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by scottbeslow on 8/17/14.
 */
public class DbConnector {

    Connection connection = null;

    public DbConnector() throws Exception {

        Class.forName("com.mysql.jdbc.Driver");
        connection = DriverManager.getConnection(Config.jdbcDatabasePath,Config.dbUser,Config.dbPassoword);

        if (connection == null) {
            System.out.println("Failed to make connection!");
            throw (new Exception("Failed to connect"));
        }

    }

    public String addEvents(List<RainEvent> rainEvents) {
        Statement statement = null;

        String sqlStatement = "INSERT INTO `hydrophilik`.`rain_event` " +
                "(`id`, `year`, `month`, `day`, `start_time`, `end_time`, `location`, `precipiation_inches`) " +
                "VALUES ";

        try {
            statement = connection.createStatement();

            for (RainEvent event : rainEvents) {
                sqlStatement = sqlStatement + event.sqlParagraph() + ",";
            }

            // Get rid of last comma
            sqlStatement = sqlStatement.substring(0, sqlStatement.length()-1);
            statement.executeUpdate(sqlStatement);

        }
        catch(Exception e) {
            e.printStackTrace();
        }


        return null;
    }

    public void close() {
        try {
            if (null != connection)
                connection.close();
        }
        catch(Exception e) {
            e.printStackTrace();;
        }
    }

    public List<RainEvent> retrieveRainEvents(String sql) throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet rs = preparedStatement.executeQuery(sql );

        List retVal = new ArrayList<RainEvent>();

        while (rs.next()) {

            long userid = rs.getLong("id");
            int year = rs.getInt("year");
            int month = rs.getInt("month");
            int day = rs.getInt("day");
            String startTime = rs.getString("start_time");
            String endTime = rs.getString("end_time");
            String location = rs.getString("location");
            double precipitationInches = rs.getDouble("precipiation_inches");

            retVal.add(new RainEvent(year, month, day, startTime, endTime, location, precipitationInches));
        }
        return retVal;
    }
}