package com.hydrophilik.rainScraper.controllers;

import com.hydrophilik.rainScraper.actor.RainEvent;
import com.hydrophilik.rainScraper.db.DbConnector;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by scottbeslow on 8/18/14.
 */
public class RainEvents {

    public static List<RainEvent> retrieveEventsByLocation(String location) {
        String sql = "SELECT * from rain_event where (location = " + location + ")";
        DbConnector dbConnector = null;

        List<RainEvent> rainEvents = null;

        try {
            dbConnector = new DbConnector();
            rainEvents = dbConnector.retrieveRainEvents(sql);

            if ((null == rainEvents) || (0 == rainEvents.size())) {
                System.out.println("Unable to find any rain events");
                dbConnector.close();
                return new ArrayList<RainEvent>(1);
            }

            return rainEvents;

        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<RainEvent>(1);
        }
        finally {
            if (null != dbConnector)
                dbConnector.close();
        }
    }

    public static List<RainEvent> retrieveAllEvents() {
        String sql = "SELECT * from rain_event";
        DbConnector dbConnector = null;

        List<RainEvent> rainEvents = null;

        try {
            dbConnector = new DbConnector();
            rainEvents = dbConnector.retrieveRainEvents(sql);

            if ((null == rainEvents) || (0 == rainEvents.size())) {
                System.out.println("Unable to find any rain events");
                dbConnector.close();
                return new ArrayList<RainEvent>(1);
            }

            return rainEvents;

        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<RainEvent>(1);
        }
        finally {
            if (null != dbConnector)
                dbConnector.close();
        }
    }
}
