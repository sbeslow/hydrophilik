package com.hydrophilik.rainScraper.utils;

import org.joda.time.DateTimeZone;

public class Config {

	public final static DateTimeZone chiTimeZone = DateTimeZone.forID("America/Chicago");
	
	public static String NCDC_URL="http://cdo.ncdc.noaa.gov/qclcd/QCLCD?prior=N";

    public static final String jdbcDatabasePath = "jdbc:mysql://localhost:8889/hydrophilik";
    public static final String dbUser = "root";
    public static final String dbPassoword = "root";
}
