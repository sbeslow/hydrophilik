package com.hydrophilik.rainScraper.utils;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public abstract class TimeUtils {

	public static DateTime createDateTimeFromCsv(String dateStr, int hour) {
		Integer year = Integer.parseInt(dateStr.substring(0,4));
		Integer month = Integer.parseInt(dateStr.substring(4, 6));
		Integer day = Integer.parseInt(dateStr.substring(6));
		
		boolean advanceDay = false;
		if (24 == hour) {
			hour = 0;
			advanceDay = true;
		}
		
		DateTime retVal = null;
		
		try {
			retVal = new DateTime(year, month, day, hour, 0, Config.chiTimeZone);
		}
		catch (Exception e) {
            e.printStackTrace();
			return null;
		}
		if (advanceDay)
			retVal = retVal.plusDays(1);
		return retVal;
	}
	
	public static String convertJodaToString(DateTime dt) {
		DateTimeFormatter fmt = DateTimeFormat.forPattern("MM/dd/yyyy HH:mm:ss");
		String str = fmt.print(dt);
		return str;
	}
	
	public static String convertTimeToString(LocalTime time) {
		DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm");
		String rawTime = fmt.print(time);
		return rawTime;
	}
	
	public static String convertDateToString(LocalDate date) {
		DateTimeFormatter fmt = DateTimeFormat.forPattern("MM/dd/yyyy");
		return fmt.print(date);
	}
	
	public static DateTime convertStringToJoda(String dtStr) {
        try {
            DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy HH:mm:ss");
            DateTime dt = formatter.parseDateTime(dtStr);
            return dt;
        }
        catch (Exception e) {
            System.out.println("Failed on " + dtStr);
            e.printStackTrace();
            return null;
        }
	}
}
